// Defines the implementations of the ADT for the stack data structure

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "stackfun.h"
#define MAX 20

int stack_arr[MAX]; // implementation of stack using an array
int top = -1; // top stores the current position of the topmost element


/**
 * @brief add an element to the top of a stack - Push Operation
 * @param data int
 * @return void
 */
void push(int data) {
    // check if stack is full
    if (isFull())
    {
        printf("Stack Overflow.");
        return;
    }
    // these lines are not executed if stack is full because of the return statement in the if clause
    top++;
    stack_arr[top] = data;
}

/**
 * @brief remove an element from the top of a stack and return it
 * @return int
 */
int pop() {
    int value;
    // check for underflow condition
    if (isEmpty())
    {
        printf("Stack Underflow.\n");
        exit(1); // statement meaning abnormal termination of a program
    }
    value = stack_arr[top];
    top--; // decrement top position
    return value; // return the value at the top
}

/**
 * @brief print all the elements in the stack
 * @return void
 */
void print() {
    int i;
    if (isEmpty())
    {
        printf("Stack Underflow.\n");
        return;
    }
    for (i = top; i >= 0; i--)
        printf("%d ", stack_arr[i]);

    printf("\n");
}

 
/**
 * @brief return a logical 1 if stack is full
 * @return int
 */
int isFull() {
    // if stack is full return 1
    if (top == MAX - 1)
        return 1;
    else
        return 0;
}


/**
 * @brief return a logical 1 if stack is empty
 * @return int
 */
int isEmpty() {
    // if stack is empty return 1
    if (top == -1)
        return 1;
    else
        return 0;
}


/**
 * @brief return the topmost element in the stack without deleting it
 * @return int
 */
int peek() {
    if (isEmpty())
    {
        printf("Stack underflow.\n");
        printf("Program terminated.");
        exit(1);
    }
    return stack_arr[top]; // the topmost element
}

