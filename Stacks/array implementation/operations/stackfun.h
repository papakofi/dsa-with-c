// Header files for the stack ADTs

#ifndef STACKFUN_H
#define STACKFUN_H

void push(int data);
int pop();
int isFull();
int isEmpty();
void print();
int peek();

#endif