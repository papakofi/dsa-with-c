// Program to implement the isFull()/isEmpty() function on a stack 

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "stackfun.h"

int main() {
    int choice, data;
    while (true)
    {
        printf("\n");
        printf("Select an operation to perform on the stack.\n");
        printf("1. Push\n");
        printf("2. Pop\n");
        printf("3. Peek\n");
        printf("4. Print the whole stack\n");
        printf("5. Quit\n");
        // Accept the choice of the user
        printf("Please enter your choice: ");
        scanf("%d", &choice);
    

    switch (choice)
    {

        case 1:
            printf("Enter the element to be pushed: ");
            scanf("%d", &data);
            push(data); // push the element into the stack
            break;

        case 2:
            data = pop();
            printf("Deleted element is %d\n", data);
            break;

        case 3:
            if (peek() == -1)
                continue;
            printf("The element at the top is %d.\n", peek());
            break;

        case 4:
            printf("This is everything in the stack:\n");
            print();
            break;

        case 5:
            print("Program ended.");
            exit(1);
            
        default:
            printf("You want wrong choice\n");
            break;
    }

    }

    return 0;
}