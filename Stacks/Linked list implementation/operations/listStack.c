// Program to perform operations on a stack implemented with linked list

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "stackList.h"



int main() {
    int choice, data, top_value, answer;
    while (true)
    {
        printf("\n1. Push\n");
        printf("2. Print\n");
        printf("3. Pop\n");
        printf("4. Peek\n");
        printf("5. Is stack empty?\n");
        printf("6. Quit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);
        printf("\n");

        switch (choice)
        {
        case 1:
            printf("Enter the element to be pushed: ");
            scanf("%d", &data);
            push(data);
            break;

        case 2:
            print();
            break;

        case 3:
            printf("The value that was popped is %d\n", pop());
            break;

        case 4:
            top_value = peek();
            printf("The value at the top is %d.\n", top_value);
            break;

        case 5:
            answer = isEmpty();
            if (answer == 0)
                printf("The stack is not empty.\n");
            else
                printf("The stack is empty.\n");
            break;

        case 6:
            exit(1);
            break;

        default:
            printf("Wrong choice\n. Try again\n");
            break;
    }
    }
    return 0;
}


