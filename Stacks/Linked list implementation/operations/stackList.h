#ifndef STACKLIST_H
#define STACKLIST_H

int isEmpty();
void push(int data);
int peek();
void print();
int pop();

#endif