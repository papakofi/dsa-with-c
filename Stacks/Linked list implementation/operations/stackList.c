// ADTs of stack implemented with linked list

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "stackList.h"

struct node {
    int data;
    struct node *link;
} *top = NULL;


// insert an element to the top of stack
void push(int data) {
    struct node *newNode;
    newNode = malloc(sizeof(newNode));
    // check if malloc is unable to allocate memory
    if (newNode == NULL)
    {
        printf("Stack overflow.");
        exit(1);
    }
    // assign the data
    newNode->data = data;
    newNode->link = NULL;

    newNode->link = top;
    top = newNode;
}

// Display the elements in the stack
void print() {
    struct node *temp;
    // use temp for traversal
    temp = top;
    printf("The stack elements are: ");
    while (temp != NULL) 
    {
        printf("%d ", temp->data);
        temp = temp->link;
    }
    
    printf("\n");
}

// Return 1 is the stack is empty
int isEmpty() {
    if (top == NULL)
        return 1;
    else
        return 0;
}

// delete the element at the top and return it
int pop() {
    struct node *temp; // to store the top pointer 
    int value; // to store value of element at the top 
    if (isEmpty()) {
        printf("Stack Underflow.");
        exit(1);
    }
    temp = top;
    value = temp->data;
    top = top->link;
    free(temp); // delete the node that top was initially pointing to
    return value;
}

// Display the value of the element at the top
int peek() {
    if (isEmpty()) {
        printf("Stack Underflow");
        exit(1);
    }
    return top->data;
    struct node *temp;
    // use temp for traversal
    temp = top;
    printf("The stack elements are: ");
    while (temp != NULL) 
    {
        printf("%d ", temp->data);
        temp = temp->link;
    }
    
    printf("\n");
}
