// Implementing a binary tree

#include <stdio.h>
#include <stdlib.h>
#include "binaryfun.h"


int main() {
    struct node *root;
    root = NULL;
    // create the binary tree
    root = create();

    // Perform preorder traversal
    printf("The preorder traversal is: ");
    preorder(root);

    // perform inorder traversal
    printf("\nThe inorder traversal of the binary tree is: ");
    inorder(root);

    return 0;
}