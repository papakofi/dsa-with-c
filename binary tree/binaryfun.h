// Function prototypes are here

#ifndef  BINARYFUN_H
#define BINARYFUN_H

struct node* create();
void preorder(struct node *root);
void inorder(struct node *root);
void postorder(struct node *root);

#endif