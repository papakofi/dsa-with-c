#include <stdlib.h>
#include <stdio.h>
#include "binaryfun.h"

/** definition for the node of a binary tree
 * @typedef {(struct node)}
 * */ 
struct node
{
    int data;
    struct node *left, *right; // set up the left and right subtrees
};

/** create individual nodes of  the tree
 */ 
struct node* create() {
    int data;
    struct node *newNode;
    newNode = (struct node *)malloc(sizeof(struct node));

    printf("Enter the data (-1 for no node): ");
    scanf("%d", &data);

    // Check if data is need or not
    if (data == -1)
        return NULL;

    newNode->data = data; 

    // Get the left child of current node
    printf("Enter the left child of %d\n", data);
    newNode->left = create();

    // Get the right child of current node
    printf("Enter right child of %d\n", data);
    newNode->right = create();
    return newNode;
}

/** Preorder traversal of the binary tree 
 * @param node root 
 * @return void 
*/
void preorder(struct node *root) {
    // end traversal if there is no subtree
    if (root == NULL)
        return;

    printf(" %d ", root->data); // print the data in the root
    preorder(root->left); // the left child of the root becomes the root for the left subtree
    preorder(root->right);
}

/** Inorder traversal of the binary tree 
 * @param struct node
 * @return void
*/
void inorder(struct node *root) {
    if (root == 0)
        return;

    inorder(root->left);
    printf(" %d ", root->data);
    inorder(root->right);
}

/** Postorder traversal of the binary tree 
 * @param struct node
 * @return void
*/
void postorder(struct node *root) {
    if (root == NULL)
        return;

    postorder(root->left);
    postorder(root->right);
    printf(" %d ", root->data);
}