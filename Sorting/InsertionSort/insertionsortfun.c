#include <stdio.h>
#include "insertionsortfun.h"

/**
 * @brief Perform insertion sort to sort data in ascending order
 * @param array array to be sorted
 * @param size the size of the array
 * @return void
 */
void insertion_sort(int array[], int size)
{
    for (int i = 1; i < size; i++)
    {
        int temp = array[i];
        int j = i - 1;
        while (j >= 0 && array[j] > temp)
        {
            array[j + 1] = array[j];
            j--;
        }
        array[j + 1] = temp;
    }
}