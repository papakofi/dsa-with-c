#include <stdio.h>
#include "bubblesortfun.h"

int main()
{
    int arr[] = {5, 1, 4, 4, 8};
    int size = sizeof(arr) / sizeof(arr[0]);
    
    bubble_sort(arr, size);

    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    return 0;
}