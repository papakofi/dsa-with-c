#ifndef MERGE_H
#define MERGE_H

void print_array(int array[], int size);
void merge_sort(int array[], int size);
void merge_sort_recursion(int array[], int lower_bound, int upper_bound);
void merge(int array[], int lower_bound, int mid, int upper_bound);

#endif