#include <stdio.h>
#include "mergesortfun.h"

// Function to print an array
void print_array(int array[], int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");
}


/** @brief Implement merge sort without specifying lower and upper bounds of the array
 * @param array array to be sorted
 * @param size size of the array to be sorted
*/
void merge_sort(int array[], int size) {
    merge_sort_recursion(array, 0, size - 1);
}

/** @brief Implement merge sort by actually specifying lower and upper bounds of the array
 * @param array array to be sorted
 * @param lower_bound lower bound of the unsorted array
 * @param upper_bound upper bound of the unsorted array
 */
void merge_sort_recursion(int array[], int lower_bound, int upper_bound) 
{
    if (lower_bound < upper_bound)
    {
        int mid = lower_bound + (upper_bound - lower_bound) / 2;
        merge_sort_recursion(array, lower_bound, mid);
        merge_sort_recursion(array, mid + 1, upper_bound);

        // execute the merging
        merge(array, lower_bound, mid, upper_bound);
    }
    
}

void merge(int array[], int lower_bound, int mid, int upper_bound) {
    int left_length = mid - lower_bound + 1;
    int right_length = upper_bound - mid;

    int temp_left[left_length]; // temporarily store the length of the left unsorted array
    int temp_right[right_length]; // temporarily store the length of the right unsorted array

    // copy the left unsorted array to temp_left
    for (int i = 0; i < left_length; i++)
        temp_left[i] = array[lower_bound + i];
    // copy the right unsorted array to temp_right
    for (int i = 0; i < right_length; i++)
        temp_right[i] = array[mid + 1 + i];

    int i, j, k; // i traverses left subarray, j traverses right subarray, k traverses the main array
    for (i = 0, j = 0, k = lower_bound; k <= upper_bound; k++)
    {
        // we copy the values from the left and right subarrays into the main array
        if ((i < left_length) && (j >= right_length || temp_left[i]  <= temp_right[j]))
        {
            array[k] = temp_left[i];
            i++;
        }
        else 
        {
            array[k] = temp_right[j];
            j++;
        }
    }
    
}
