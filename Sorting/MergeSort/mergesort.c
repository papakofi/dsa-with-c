#include <stdio.h>
#include "mergesortfun.h"

int main() {
    int array[] = {9, 4, 8, 1, 7, 0, 3, 2, 5, 6};
    int size = sizeof(array) / sizeof(array[0]);

    printf("Unsorted array: ");
    print_array(array, size);

    merge_sort(array, size);

    printf("Sorted array: ");
    print_array(array, size);

    return 0;
}

// function to implement merge sort
