#include <stdio.h>
#include "quicksortfun.h"

/**
 * @brief function to swap two values
 * @param a first value
 * @param b second value
 * @return void
 */
void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

/**
 * @brief Partition function essentially takes a pivot value and arranges smaller values than it to its left and greater values
 * than it to its right.
 * @param array array to be sorted
 * @param lower_bound lower bound of the array
 * @param upper_bound upper bound of the array
 * @return int - a new pivot value
 */
int partition(int array[], int lower_bound, int upper_bound)
{
    int pivot = array[upper_bound];
    int i = lower_bound;
    for (int j = lower_bound; j < upper_bound; j++)
    {
        if (array[j] <= pivot)
        {
            swap(&array[i], &array[j]);
            i++;
        }
    }
    swap(&array[i], &array[upper_bound]);
    return i;
}

// Quick sort function
void quick_sort(int array[], int lower_bound, int upper_bound)
    {
        if (lower_bound < upper_bound)
        {
        // Partition the array and get pivot index
        int pivot_index = partition(array, lower_bound, upper_bound);
        // Recursively sort elements before pivot
        quick_sort(array, lower_bound, pivot_index - 1);
        // Recursively sort elements after pivot
        quick_sort(array, pivot_index + 1, upper_bound);
    }
}

// Function to print an array
void print_array(int array[], int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");
}