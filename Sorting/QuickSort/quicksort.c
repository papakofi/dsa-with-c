#include <stdio.h>
#include "quicksortfun.h"

int main()
{
    int array[] = {64, 25, 12, 22, 11};
    int size = sizeof(array) / sizeof(array[0]);

    printf("Original array: ");
    print_array(array, size);

    quick_sort(array, 0, size - 1);

    printf("Sorted array: ");
    print_array(array, size);

    return 0;
}