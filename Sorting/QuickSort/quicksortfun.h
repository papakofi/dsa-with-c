#ifndef QUICK_H
#define QUICK_H

void print_array(int array[], int size);
void quick_sort(int array[], int lower_bound, int upper_bound);
void swap(int *a, int *b);
int partition(int array[], int lower_bound, int uppper_bound);

#endif