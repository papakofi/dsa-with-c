#ifndef SELECTION_H
#define SELECTION_H

void swap(int *a, int *b);
void selection_sort(int *array, int size);
void print_array(int array[], int size);

#endif