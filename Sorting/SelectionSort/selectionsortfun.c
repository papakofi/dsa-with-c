#include <stdio.h>
#include "selectionsortfun.h"

/**
 * @brief function to swap two values
 * @param a first value
 * @param b second value
 * @return void
 */
void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

/**
 * @brief Perform selection sort to sort data in ascending order
 * @param array array to be sorted
 * @param size the size of the array
 * @return void
 */
void selection_sort(int *array, int size)
{
    for (int i = 0; i < size; i++)
    {
        // Find the minimum element in the unsorted array
        int min = i;
        for (int j = i + 1; j < size; j++)
        {
            if (array[j] < array[min])
            {
                min = j;
            }
        }
        // Swap the found minimum element with the first element
        swap(&array[min], &array[i]);
    }
}

// Function to print an array
void print_array(int array[], int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");
}