#include <stdio.h>
#include "selectionsortfun.h"


int main()
{
    int array[] = {64, 25, 12, 22, 11, 12};
    int size = sizeof(array) / sizeof(array[0]);

    printf("Original array: ");
    print_array(array, size);

    selection_sort(array, size);

    printf("Sorted array: ");
    print_array(array, size);

    return 0;
}