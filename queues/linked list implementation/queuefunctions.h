// Function signatures for the queue data structure
#ifndef QUEUEFUNCTIONS_H
#define QUEUEFUNCTIONS_H

void enqueue(int data);
void dequeue();
int isEmpty();
void peek();
void print();

#endif