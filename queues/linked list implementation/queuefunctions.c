// Implementations for the queue data structure

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "queuefunctions.h"

struct node {
    int data;
    struct node *link;
} *front = NULL, *rear = NULL; // front and rear pointers

// add an element to the queue
void enqueue(int data) {
    struct node *newNode;
    newNode = malloc(sizeof(struct node));
    newNode->data = data;
    newNode->link = NULL;

    // check if the queue is empty
    if (isEmpty())
        front = rear = newNode;

    else {
        rear->link = newNode;
        rear = newNode;
    }
}

// display the elements of the queue
void print() {
    struct node *temp; // pointer for traversal
    temp = NULL;
    if (isEmpty()) // check is queue is empty
        printf("Queue is empty.");
    else {
        temp = front;
        printf("The elements are: ");
        while (temp != NULL)
        {
            printf("%d ", temp->data);
            temp = temp->link;
        }
            
    }
}

// return 1 if queue is empty
int isEmpty() {
    if (front==NULL && rear==NULL)
        return 1;
    else
        return 0;
}

// remove elements from the queue
void dequeue() {
    struct node *temp;
    temp = front;
    if (isEmpty()) // check for underflow condition
        printf("queue is already empty");
    else {
        printf("The element removed is %d", front->data);
        front = front->link;
        free(temp);
    }  
}

// check the element at the front of the queue
void peek() {
    if (isEmpty())   // check for underflow condition
        printf("queue is already empty");
    else {
        printf("The element at the top is %d", front->data);
    }
}