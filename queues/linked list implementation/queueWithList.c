// Program to perform queue operations on a queue using linked list implementation

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "queuefunctions.h"


int main()
{
    int choice, data, top_value, answer;
    while (true)
    {
        printf("\n1. Enqueue\n");
        printf("2. Dequeue\n");
        printf("3. Display the elements\n");
        printf("4. Peek\n");
        printf("5. Is queue empty?\n");
        printf("6. Quit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);
        printf("\n");

        switch (choice)
        {
        case 1:
            printf("Enter the element to be pushed: ");
            scanf("%d", &data);
            enqueue(data);
            break;

        case 2:
            dequeue();
            break;

        case 3:
            print();
            break;

        case 4:
            peek();
            break;

        case 5:
            answer = isEmpty();
            if (answer == 0)
                printf("The queue is not empty.\n");
            else
                printf("The queue is empty.\n");
            break;

        case 6:
            exit(1);
            break;

        default:
            printf("Wrong choice\n. Try again\n");
            break;
    }
  }
}


