// Queue ADTs 

#ifndef QUEUEFUN_H
#define QUEUEFUN_H

void enqueue(int data);
int isFull();
int isEmpty();
void dequeue();
void print();
void peek();

#endif