// Queue ADTs using array implementation

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "queuefun.h"

#define MAX 5

int front = -1;
int rear = -1;
int queue[MAX];

// print the element at the front without deletion
void peek() {
    // check for underflow
    if (isEmpty())
        printf("Underflow condition.");
    else {
        printf("The element at the front is %d ", queue[front]);
    }

}

// delete a value in the element and return it
void dequeue() {
    // check for underflow
    if (isEmpty())
        printf("Underflow condition.");
    // check if queue contains only one element
    else if (front == rear)
    {
        printf("The dequeued value is %d", queue[front]);
        front = rear = -1;
    }
    else {
        printf("The dequeued value is %d", queue[front]);
        front++;
    }
    
}

// display the elements of a queue
void print() {
    if (isEmpty())
        printf("Underflow condition");
    else {
        printf("The elements of the queue: ");
        for (int i = front; i < rear + 1; i++)
            printf("%d ", queue[i]);
    }
}

// add an element to the queue
void enqueue(int data) {
    // Check if the queue is full
    if (isFull())
        printf("Overflow");
    // Check if the queue is empty
    else if (isEmpty())
    {
        front = rear = 0;
        queue[rear] = data;
    }
    else 
    {
        rear++;
        queue[rear] = data;
    }
}

// Return 1 if queue is full
int isFull() {
    if (rear == MAX - 1)
        return 1;
    else
        return 0;
}

// Return 1 if queue is empty
int isEmpty() {
    if (front == -1 && rear == -1)
        return 1;
    else
        return 0;
}

