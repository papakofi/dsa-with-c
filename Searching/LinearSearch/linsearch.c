#include <stdio.h>
#include "linearsearchfun.h"

int main()
{
    int arr[] = {2, 4, 6, 8, 10};
    int size = sizeof(arr) / sizeof(arr[0]);
    int x = 4;
    int result = linearsearch(arr, x, size);
    if (result == -1)
    {
        printf("Element not found in the array.\n");
    }
    else
    {
        printf("Element found at index %d.\n", result);
    }
    return 0;
}
