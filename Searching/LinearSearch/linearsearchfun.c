// Implementation of linear search

#include <stdio.h>
#include "linearsearchfun.h"

/**
 * @brief Perform a linear search on an array
 * @param data item to search for
 * @param array array to search in
 * @param size the size of the array
 * @return int - return the index of element if found otherwise return -1
 */
int linearsearch(int array[],int data, int size) {
    int i;
    for (i = 0; i < size; i++)
    {
        if (array[i] == data)
            return i;   
    }
    // if not found
        return -1;
}
