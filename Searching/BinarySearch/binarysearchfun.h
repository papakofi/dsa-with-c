// binary search functions

#ifndef BINARYFUN_H
#define BINARYFUN_H

int binary_search(int array[], int data, int size);

#endif