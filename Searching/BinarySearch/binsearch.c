#include <stdio.h>
#include "binarysearchfun.h"
#include "bubblesortfun.h"


int main()
{
    int arr[] = {4, 2, 8, 6, 12, 10};
    int size = sizeof(arr) / sizeof(arr[0]);
    int x = 10;
    bubble_sort(arr, size); // binary search is performed on sorted array

    int result = binary_search(arr, size, x);
    if (result == -1) 
    {
        printf("Element not found in the array.\n");
    }
    else
    {
        printf("Element found at index %d.\n", result);
    }
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    return 0;
}