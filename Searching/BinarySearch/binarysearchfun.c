#include <stdio.h>
#include "binarysearchfun.h"

/**
 * @brief Perform a binary search on an array
 * @param array array to search in
 * @param size the size of the array
 * @param data item to search for
 * @return int - return the index of data element if found otherwise return -1
 */
int binary_search(int arr[], int size, int data)
{
    int left = 0;
    int right = size - 1;
    while (left <= right)
    {
        int mid = (left + right) / 2;
        if (arr[mid] == data)
        {
            return mid;
        }
        else if (data < arr[mid])
        {
            right = mid - 1;
        }
        else
        {
            left = mid + 1;
        }
    }
    return -1;
}
