#include <stdio.h>
#include "bubblesortfun.h"

/**
 * @brief Perform bubble sort to sort data in ascending order
 * @param array array to sort
 * @param size the size of the array
 * @return void
 */
void bubble_sort(int arr[], int size)
{
    int flag;
    for (int i = 0; i < size - 1; i++)
    {
        flag = 0;
        for (int j = 0; j < size - i - 1; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
                flag = 1;
            }
        }
        if (flag == 0)
            break;  
    }
}
