// Creation of single linked list containing three nodes

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int data;
    struct node *link;
};

int main() {
    // Create the head pointer and allocate memory for it(we allocate heap memory)
    struct node *head = NULL;
    head = (struct node *)malloc(sizeof(struct node));

    // Creation of the first node
    (*head).data = 45;
    head->link = NULL;

    // Create another block of memory to store a second node
    struct node *current = NULL;
    current = (struct node *)malloc(sizeof(struct node));

    // the data in this node is as follows
    (*current).data = 95;
    current->link = NULL;

    // update the first node to store the address of the second node
    head->link = current;

    // Create another block of memory to store the third node
    struct node *current2 = NULL;
    current2 = malloc(sizeof(struct node));

    current2->data = 43;
    current2->link = NULL;
    //store the address of the third node in the second node
    current->link = current2;


    printf("%d\n", head->data);

    return 0;
}