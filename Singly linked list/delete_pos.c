// Program to delete the a node at a specified position of a singly linked list

#include <stdio.h>
#include <stdlib.h>

struct node *add_at_end(struct node *ptr, int data);
void del_pos(struct node **head, int position);

struct node
{
    int data;
    struct node *link;
};

int main() {
    // creation of the linked list using the insertion2.c file
    struct node *head = malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    struct node *ptr = head;
    ptr = add_at_end(ptr, 98);
    ptr = add_at_end(ptr, 3);
    ptr = add_at_end(ptr, 67);

    ptr = head;

    // Traverse the list and print the data
    printf("The data in the list before deletion of  the second node: \n");
    while(ptr != NULL)
    {
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }

    // Delete the 2rd node
    int position = 2;
    del_pos(&head, position);
    ptr = head;

    // Traverse the list and print the data
    printf("\nThe data in the list after deletion of the second node: \n");
    while(ptr != NULL)
    {
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }
    return 0;
}

void del_pos(struct node **head, int position) {
    // the current pointer points to the node we want to delete
    // the previous pointer will point to the node just before the node we want to delete
    struct node *current = *head;
    struct node *previous = *head;

    if(*head == NULL) // check for underflow condition
        printf("List is empty.(UNDERFLOW)");
    else if(position == 1) // if the position happens to be the first
    {
        *head = current->link;
        free(current);
        current = NULL;
    }
    else {
        while (position != 1)
        {
            previous = current;
            current = current->link;
            position--;
        } 
    }
    previous->link = current->link;
    free(current);
    current = NULL;
}


struct node *add_at_end(struct node *ptr, int data) {
    struct node *temp = malloc(sizeof(struct node));
    temp->data = data;
    temp->link = NULL;

    ptr->link = temp;
    return temp;
}

