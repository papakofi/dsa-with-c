// Program to delete the last  node of a singly linked list(alternative 2)

#include <stdio.h>
#include <stdlib.h>

struct node *add_at_end(struct node *ptr, int data);
void del_last(struct node *head);

struct node
{
    int data;
    struct node *link;
};

int main() {
    // creation of the linked list using the insertion2.c file
    struct node *head = malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    struct node *ptr = head;
    ptr = add_at_end(ptr, 98);
    ptr = add_at_end(ptr, 3);
    ptr = add_at_end(ptr, 67);

    ptr = head;

    // Traverse the list and print the data
    printf("The data in the list before deletion of last node: \n");
    while(ptr != NULL)
    {
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }

    // Delete the first node
    del_last(head);
    ptr = head;

    // Traverse the list and print the data
    printf("\nThe data in the list after deletion of last node: \n");
    while(ptr != NULL)
    {
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }
    return 0;
}

struct node *add_at_end(struct node *ptr, int data) {
    struct node *temp = malloc(sizeof(struct node));
    temp->data = data;
    temp->link = NULL;

    ptr->link = temp;
    return temp;
}

void del_last(struct node *head) {
    // first check for underflow
    if (head == NULL)
        printf("List is empty(UNDERFLOW)");
    // Is there a single node in the list?
    else if (head->link == NULL)
    {
        // delete head
        free(head);
        head = NULL;
    }
    else 
        {
            // set-up a temp variable for traversing
            struct node *temp = head;
            // we want temp to stop at the last but one node
            while (temp->link->link != NULL)
                temp = temp->link;
            
            // delete the node after the last but one node
            free(temp->link);
            temp->link = NULL;
        }
}

