// Program to count the number of nodes in a linked list

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int data;
    struct node *link;
};

void node_count(struct node *head);

int main() {
    // Create the head pointer which points to the first node 
    struct node *head = NULL;
    head = (struct node *)malloc(sizeof(struct node));
    // Supply the data and the link value
    (*head).data = 45;
    head->link = NULL;

    // Create another block of memory to store a second node
    struct node *current = NULL;
    current = (struct node *)malloc(sizeof(struct node));
    // Supply the data and the link value
    (*current).data = 95;
    current->link = NULL;

    // update the first node to store the address of the second node
    head->link = current;

    // Using the same pointer current again
    current = malloc(sizeof(struct node));
    current->data = 78;
    current->link = NULL;
    
    // store the address of third node in second node
    head->link->link = current;

    // creating a fourth node
    current = malloc(sizeof(struct node));
    current->data = 56;
    current->link = NULL;

    // store the address of the fourth node in the third node
    head->link->link->link = current;

    // call the node_count function
    node_count(head);

    return 0;
}

// a function to count the number of nodes in the linked list

void node_count(struct node *head) {
    int count = 0;
    if (head == NULL)
        printf("Linked list is empty(UNDERFLOW)");
    // initialize another pointer(ptr) for the traversing
    else {
    struct node *ptr = NULL;
    ptr = head;
    while (ptr != NULL)
    {
        count++;
        // move to the next node
        ptr = ptr->link;
    }
    printf("Number of nodes: %d", count);
    }
}