// Creation of a node in C

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int data;
    struct node *link;
};

int main() {
    // Create the head pointer and allocate memory for it(we allocate heap memory)
    struct node *head = NULL;
    head = (struct node *)malloc(sizeof(struct node));

    (*head).data = 5;
    head->link = NULL;

    printf("%d", head->data);
    return 0;
}
