// Program to add a node at the beginning of a singly linked list

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int data;
    struct node *link;
};

struct node *add_begin(struct node *head, int data);

int main() { // create first node
    struct node *head = malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    // create the second node
    struct node *ptr = malloc(sizeof(struct node));
    ptr->data = 98;
    ptr->link = NULL;

    head->link = ptr;

    int data = 3; // new data to add

    head = add_begin(head, data);
    ptr = head;

    head = add_begin(head, 7);
    ptr = head;
    
    while (ptr != NULL)
    {
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }
    return 0;
}

struct node *add_begin(struct node *head, int data) {
    struct node *ptr = malloc(sizeof(struct node));
    ptr->data = data;
    ptr->link = NULL;

    // assign the ptr, head
    ptr->link = head;
    head = ptr;
    return head;

}

