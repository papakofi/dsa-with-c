// Program to add a node at the end of a singly linked list

#include <stdio.h>
#include <stdlib.h>


struct node
{
    int data;
    struct node *link;
};

void add_at_end(struct node *head, int);
void print_data(struct node *head);

int main() {
    // Create the head pointer which points to the first node 
    struct node *head = NULL;
    head = (struct node *)malloc(sizeof(struct node));
    // Supply the data and the link value
    (*head).data = 45;
    head->link = NULL;

    add_at_end(head, 67);

    // display the new linked list
    print_data(head);
}

void add_at_end(struct node *head, int data) {
    struct node *ptr, *temp;
    ptr = head;
    temp = (struct node *)malloc(sizeof(struct node));

    // new node with the new data and link being NULL
    temp->data = data;
    temp->link = NULL;

    while (ptr->link != NULL)
    {
        // Update ptr by the link of each node in order to traverse
        ptr = ptr->link;
    }
    // the link at the last but one node gets updated to temp
    ptr->link = temp;
}

void print_data(struct node *head) {
    if (head == NULL)
        printf("Linked list is empty.(UNDERFLOW)");
    // initialize another pointer(ptr) for the traversing
    else {
    struct node *ptr = NULL;
    ptr = head;
    while (ptr != NULL)
    {
        // move to the next node
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }
    
    }
}