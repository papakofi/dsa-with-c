// Creation of single linked list containing three nodes(Method 2)

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int data;
    struct node *link;
};

int main() {
    // Create the head pointer and allocate memory for it(we allocate heap memory)
    struct node *head = NULL;
    head = (struct node *)malloc(sizeof(struct node));

    // Creation of the first node
    (*head).data = 45;
    head->link = NULL;
    // Create another block of memory to store a second node
    struct node *current = NULL;
    current = (struct node *)malloc(sizeof(struct node));
    printf("%d\n", head->data);

    // the data in this node is as follows
    (*current).data = 95;
    current->link = NULL;
    // update the first node to store the address of the second node
    head->link = current;
    printf("%d\n", current->data);

    // Using the same pointer current again
    current = malloc(sizeof(struct node));
    current->data = 78;
    current->link = NULL;
    printf("%d\n", current->data);
    
    // store the address of third node in second node
    head->link->link = current;

    

    return 0;
}