// Program to delete a singly linked list entirely

#include <stdio.h>
#include <stdlib.h>

struct node *add_at_end(struct node *ptr, int data);
struct node *del_list(struct node *head);

struct node
{
    int data;
    struct node *link;
};

int main() {
    // creation of the linked list using the insertion2.c file
    struct node *head = malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    struct node *ptr = head;
    ptr = add_at_end(ptr, 98);
    ptr = add_at_end(ptr, 3);
    ptr = add_at_end(ptr, 67);

    ptr = head;

    // Traverse the list and print the data
    printf("The data in the list before deletion of  the entire node: \n");
    while(ptr != NULL)
    {
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }

    // Delete the entire node
    printf("\nDeleting the entire list...");
    head = del_list(head);
    if (head == NULL)
        printf("\nLinked list deleted successfully.");
}

struct node* del_list(struct node *head) {
    struct node *temp = malloc(sizeof(struct node));
    temp = head;

    // delete each node one by one until temp == NULL
    while (temp != NULL) 
    {
        temp = temp->link;
        free(head);
        head = temp;
    }
    return head;
}

struct node *add_at_end(struct node *ptr, int data) {
    struct node *temp = malloc(sizeof(struct node));
    temp->data = data;
    temp->link = NULL;

    ptr->link = temp;
    return temp;
}

