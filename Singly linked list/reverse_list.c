// Program to reverse an entire linked list

#include <stdio.h>
#include <stdlib.h>

struct node *add_at_end(struct node *ptr, int data);
struct node *reverse(struct node *head);

struct node
{
    int data;
    struct node *link;
};

int main() {
    // creation of the linked list using the insertion2.c file
    struct node *head = malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    struct node *ptr = head;
    ptr = add_at_end(ptr, 98);
    ptr = add_at_end(ptr, 3);
    ptr = add_at_end(ptr, 67);

    ptr = head;

    // Traverse the list and print the data
    printf("The data in the list before the reversal: \n");
    while(ptr != NULL)
    {
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }

    head = reverse(head);
    ptr = head;

    // Traverse the list and print the data
    printf("\nThe data in the list after the reversal: \n");
    while(ptr != NULL)
    {
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }
    return 0;
}



struct node* reverse(struct node *head) {
    // prev is a pointer that lags behind the head pointer
    // next is a pointer that leads the head pointer
    struct node *prev = NULL;
    struct node *next = NULL;
    while (head != NULL)
    {
        next = head->link;
        head->link = prev;
        prev = head;
        head = next;
    }
    head = prev;
    return head;
}

struct node *add_at_end(struct node *ptr, int data) {
    struct node *temp = malloc(sizeof(struct node));
    temp->data = data;
    temp->link = NULL;

    ptr->link = temp;
    return temp;
}

